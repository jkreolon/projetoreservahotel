<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="ISO-8859-1"%>
<%@ page contentType="text/html; charset=utf-8" language="java" import="br.unipe.cc.model.Categoria" errorPage="" %>
<%@ include file="main.jsp"%>      
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nova Categoria</title>
</head>
<body>
 <% 
  Categoria categoria = (Categoria) request.getAttribute("categoria");
 %>
 
<div class="container">
<br />
  <form method="post" action="cadastrarCategoria.do" class="form-horizontal" role="form">
    <div class="form-group form-group-sm">
      <input type="hidden" name="id" value="<%= categoria != null ? categoria.getId() : "" %>">
      <label class="col-sm-2 control-label" for="sm">Descricao:</label>
      <div class="col-sm-10">
        <input class="form-control" placeholder="Insira a descricao" type="text" name="nome" value="<%= categoria != null ? categoria.getNome() : "" %>" id="sm">
      </div>
      <label class="col-sm-2 control-label" for="sm">Capacidade:</label>
      <div class="col-sm-10">
        <input class="form-control" placeholder="Insira a Capacidade" type="text" name="capacidade" value="<%= categoria != null ? categoria.getCapacidade() : "" %>" id="sm">
      </div>
      <label class="col-sm-2 control-label" for="sm">Valor:</label>
      <div class="col-sm-10">
        <input class="form-control" placeholder="Insira o Valor" type="text" name="valor" value="<%= categoria != null ? categoria.getValor() : "" %>" id="sm">
      <br />
      <button type="submit" class="btn btn-default">Adicionar</button>                     
      </div>
    </div>    
  </form>
</div>
</body>
</html>
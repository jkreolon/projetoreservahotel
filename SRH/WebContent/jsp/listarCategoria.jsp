<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="ISO-8859-1"%>
<%@ page contentType="text/html; charset=utf-8" language="java" import="br.unipe.cc.model.Categoria" errorPage="" %>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.List" errorPage="" %>    
<%@ include file="/jsp/main.jsp"%>    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset=utf-8">
<title>Categoria</title>
</head>
<body>
<% if(request.getAttribute("msg") != null){%> 
   <%=request.getAttribute("msg")%>
<% }%>
<br />
  <a href='novaCategoria.do'>Novo</a>
  <a href='logout'>Logout</a>
  <br />
  <br />
  <table>
  <tr>
  <td>
  Codigo da categoria
  </td>
  <td>
  Nome
  </td>
  <td>
  Capacidade
  </td>
  <td>
  A��es
  </td>
  </tr>
    <%
    List<Categoria> categorias =  (List<Categoria>) request.getAttribute("categorias");
    
    for(Categoria categoria : categorias){ %>
    
    <tr>
    <td>
      <%=categoria.getId()%>
    </td>
    <td>
      <%=categoria.getNome()%>
    </td>
    <td>
    <%=categoria.getCapacidade()%>
    </td>
    <td>
    <a href="removerCategoria.do?id=<%=categoria.getId() %>">remover</a>
    <a href="novaCategoria.do?id=<%=categoria.getId() %>">editar</a>
    </td>
    </tr>
    <%}%>
    
  </table>

  
</body>
</html>
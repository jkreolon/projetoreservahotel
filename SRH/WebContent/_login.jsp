<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Tela de login</title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>

<% if(request.getAttribute("msgErro") != null){%> 
   <%=request.getAttribute("msgErro")%>
<% }%>

  <div>
  <form method="post" action="mainAcess">
	  <fieldset id="fieldset">
	    <legend>Login</legend>
	      <div class="LineForm">
	        <label>Login:</label>
	        <input type="text" name="login" />
	      </div>
	      <div>
	        <label>Senha:</label>
	          <input type="password" name="senha" />
	      </div> 
	      <div> 
	        <input type="submit" value="Logar" />          
	      </div>
	      </fieldset>
	</form>
  </div>
   
</body>
</html>
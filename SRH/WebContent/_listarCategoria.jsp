<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="ISO-8859-1"%>
<%@ page contentType="text/html; charset=utf-8" language="java" import="br.unipe.cc.model.Categoria" errorPage="" %>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.List" errorPage="" %>    
<%@ include file="main.jsp"%>    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset=utf-8">
<title>Categoria</title>
</head>
<body>
<% if(request.getAttribute("msg") != null){%> 
   <%=request.getAttribute("msg")%>
<% }%>
<br />
<br />
<div class="container">

  <form class="form-inline">
    <div class="form-group">
      <label for="exampleInputEmail1">Nome: </label>
      <input class="form-control" placeholder="Insira a categoria">
      <button type="submit" class="btn btn-default">Pesquisar</button>
    </div>
  </form>  

  <br />
  <form method="get" action="novaCategoria.do">
    <div> 
      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="Inserir nova Categoria">Novo</button>
    </div>
  </form>
  <br />
  
  <div class="table-responsive">
    <table class="table">
      <tr>
        <td>Codigo</td>
        <td>Descricao</td>
        <td>Capacidade</td>
        <td>Valor</td>
        <td></td>
      </tr>
        <% 
        List<Categoria> categorias =  (List<Categoria>) request.getAttribute("categorias");
    
        for(Categoria categoria : categorias){ %>      
      <tr>
        <td><%=categoria.getId()%></td>
        <td><%=categoria.getNome()%></td>
        <td><%=categoria.getCapacidade()%></td>
        <td><%=categoria.getValor()%></td>
        <td>
          <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              Opcao <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a href="removerCategoria.do?id=<%=categoria.getId() %>">Remover</a></li>
              <li><a href="novaCategoria.do?id=<%=categoria.getId() %>">Editar</a></li>
            </ul>
          </div>    
        </td>
      </tr>
        <%}%>      
    </table>
  </div>
 

</div>  
</body>
</html>
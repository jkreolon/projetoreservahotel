<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repeti��o e setar vari�veis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formata��es -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<!DOCTYPE html">
<html>
<head>
<script src="../../bootstrap_v3_3_4/jquery-1.11.3.min.js"> </script>
<script src="../../bootstrap_v3_3_4/js/bootstrap.js"> </script>
<link href="../../bootstrap_v3_3_4/css/bootstrap.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar Quarto</title>
</head>
<body>
<div class="container">

<!-- Cabe�alho -->
<div class="page-header">
  <h1>Controle de Reservas</h1> 
  <small>
    Bem Vindo: ${usuario.login}
  </small>
</div>

<!-- Main -->
<div>
  <ul class="nav nav-tabs">
    <li role="presentation"><a href="/SRH/categoria/listar">Categorias</a></li>
    <li role="presentation"><a href="listar">Quartos</a></li>
    <li role="presentation"><a href="/SRH/hospede/listar">Hospedes</a></li>
    <li role="presentation"><a href="/SRH/reserva/listar">Reservas</a></li>
    <li role="presentation"><a href="#">Checar Disponibilidade</a></li>
    <li role="presentation"><a href="/SRH/logout">Sair</a></li>
  </ul>
</div> 

<br />
<br />

  <!-- Preenchimento dos campos Quarto -->
  <c:url var="url" value="/quarto/update"/>
  <form:form class="form-horizontal" role="form" action="${url}" method="POST" modelAttribute="quarto">
    <div class="form-group form-group-sm">
      <label class="col-sm-2 control-label" for="sm">Categoria:</label>
      <form:hidden path="id"/>
      <div class="col-sm-10">
        <form:select class="col-sm-2 control-label" path="categoria.id" items="${selectCategoria}"/>
        <form:errors  path="categoria.id"/>
        <br />
      </div>      
      <label class="col-sm-2 control-label" for="sm">Andar:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="andar" placeholder="Insira o andar"/>
        <form:errors  path="andar"/>
      </div>
      <label class="col-sm-2 control-label" for="sm">N�mero:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="numero" placeholder="Insira o numero do apto"/>
        <form:errors path="numero"/>
      <br />      
      <input class="btn btn-default" type="submit" value="Atualizar"/>    
      </div>
    </div>    
  </form:form>

<!-- Sem formatacao
	<c:url var="url" value="/quarto/update"/>
	<form:form action="${url}" method="POST" modelAttribute="quarto">
	  <form:hidden path="id"/> 
	  Categoria:<form:select path="categoria.id" items="${selectCategoria}"/>
    <form:errors path="categoria"/>
		<br />
		Andar:<form:input path="andar"/>
		<form:errors path="andar"/>
		<br />
		Numero:<form:input path="numero"/>
		<form:errors path="numero"/>
		<br/>
		<input type="submit" value="Atualizar"/>
		<form:hidden path="id"/>
	</form:form>
 -->
</div>  
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repeti��o e setar vari�veis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formata��es -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<script src="bootstrap_v3_3_4/jquery-1.11.3.min.js"> </script>
<script src="bootstrap_v3_3_4/js/bootstrap.js"> </script>
<link href="bootstrap_v3_3_4/css/bootstrap.css" rel="stylesheet">

<meta http-equiv="Content-Type" content="text/html" charset=utf-8">
<title>Sistema de Reserva</title>
</head>
<body>
<div class="container">

<div class="page-header">
  <h1>Controle de Reservas</h1> 
  <small>
    Bem Vindo: ${usuario.login}
  </small>
</div>

<div>
  <ul class="nav nav-tabs">
    <li role="presentation"><a href="categoria/listar">Categorias</a></li>
    <li role="presentation"><a href="quarto/listar">Quartos</a></li>
    <li role="presentation"><a href="hospede/listar">Hospedes</a></li>
    <li role="presentation"><a href="reserva/listar">Reservas</a></li>
    <li role="presentation"><a href="#">Checar Disponibilidade</a></li>
    <li role="presentation"><a href="logout">Sair</a></li>
  </ul>
</div> 

</div>

<!-- 
<div class="LineForm">
  <fieldset id="fieldset">
    <legend>Controle de Reservas - Copyright 2015</legend>
    <p class="text-center">Curso de Sistemas para Internet</p>
    <p class="text-center">Centro Universit�rio de Jo�o Pessoa</p>
  </fieldset>
</div>    
-->

</body>
</html>
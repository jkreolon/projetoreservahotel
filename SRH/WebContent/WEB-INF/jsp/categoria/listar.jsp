<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repeti��o e setar vari�veis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formata��es -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<!DOCTYPE html>
<html>
<head>
<script src="../bootstrap_v3_3_4/jquery-1.11.3.min.js"> </script>
<script src="../bootstrap_v3_3_4/js/bootstrap.js"> </script>
<link href="../bootstrap_v3_3_4/css/bootstrap.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Listagem de Categorias</title>
</head>
<body>
<div class="container">

<!-- Cabe�alho -->
<div class="page-header">
  <h1>Controle de Reservas</h1> 
  <small>
    Bem Vindo: ${usuario.login}
  </small>
</div>

<!-- Main -->
<div>
  <ul class="nav nav-tabs">
    <li role="presentation"><a href="listar">Categorias</a></li>
    <li role="presentation"><a href="/SRH/quarto/listar">Quartos</a></li>
    <li role="presentation"><a href="/SRH/hospede/listar">Hospedes</a></li>
    <li role="presentation"><a href="/SRH/reserva/listar">Reservas</a></li>
    <li role="presentation"><a href="#">Checar Disponibilidade</a></li>
    <li role="presentation"><a href="/SRH/logout">Sair</a></li>
  </ul>
</div> 
${msg}
<br />
<br />


<!-- Listagem da Categoria -->
<c:url var="urlFiltro" value="/categoria/pesquisalike" />
  <form:form class="form-inline" action="${urlFiltro}" method="GET" modelAttribute="filtro">
    <div class="form-group">
      <label>Nome: </label>
      <form:input class="form-control" path="nome" placeholder="Insira a categoria"/>
      <button type="submit" class="btn btn-default">Pesquisar</button>
    </div>
  </form:form>   

<br />
<br />

<div class="btn-group" role="group" aria-label="...">
  <form:form method="get" action="form">
    <button type="submit" class="btn btn-default" >Novo</button>
  </form:form>  
</div>

  <div class="table-responsive">
    <table class="table">
      <tr>
        <td>Codigo</td>
        <td>Descricao</td>
        <td>Capacidade</td>
        <td>Valor</td>
        <td></td>
      </tr>
      <c:forEach var="categoria" items="${categorias}">  
      <tr>
        <td>${categoria.id}</td>
        <td>${categoria.nome}</td>
        <td>${categoria.capacidade}</td>
        <td>${categoria.valor} </td>
        <td>
          <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              Opcao <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <c:url var="url" value="/categoria/${categoria.id}/remover"/>
              <li><a href="${url}">Remover</a></li>
              <c:url var="url" value="/categoria/${categoria.id}/form"/>
              <li><a href="${url}">Editar</a></li>
            </ul>
          </div>    
        </td>
      </tr>
      </c:forEach> 
    </table>
  </div>
</div>

  <!-- Modo sem formatacao -->
  <!-- 
	<c:url var="url" value="form"/>
	<a href='${url}'>Novo</a>
  <c:url var="url" value="/main"/>	
	 <a href='${url}'>Voltar</a>

<br />
<br />
	<table>
	<tr>
	 <td>
	   C�digo da categoria
	 </td>
	 <td>
	   Nome
	 </td>
	 <td>
	   Capacidade
	 </td>
	 <td>
	   A��es
	 </td>
	</tr>
		<c:forEach var="categoria" items="${categorias}">
		<tr>
		  <td>
			 ${categoria.id}
		  </td>
		  <td>
			 ${categoria.nome}
		  </td>
		  <td>
		    ${categoria.capacidade} 
		  </td>
		  <td>
		    <c:url var="url" value="/categoria/${categoria.id}/remover"/>
		    <a href="${url}">remover</a>
		    <c:url var="url" value="/categoria/${categoria.id}/form"/>
		    <a href="${url}">editar</a>
		  </td>
		</tr>
		</c:forEach>
		
	</table>
  -->

</body>
</html>
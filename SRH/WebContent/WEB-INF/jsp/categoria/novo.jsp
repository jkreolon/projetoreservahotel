<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repeti��o e setar vari�veis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formata��es -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<!DOCTYPE html>
<html>
<head>
<script src="../bootstrap_v3_3_4/jquery-1.11.3.min.js"> </script>
<script src="../bootstrap_v3_3_4/js/bootstrap.js"> </script>
<link href="../bootstrap_v3_3_4/css/bootstrap.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nova categoria</title>
</head>
<body>
<div class="container">

<!-- Cabe�alho -->
<div class="page-header">
  <h1>Controle de Reservas</h1> 
  <small>
    Bem Vindo: ${usuario.login}
  </small>
</div>

<!-- Main -->
<div>
  <ul class="nav nav-tabs">
    <li role="presentation"><a href="listar">Categorias</a></li>
    <li role="presentation"><a href="/SRH/quarto/listar">Quartos</a></li>
    <li role="presentation"><a href="/SRH/hospede/listar">Hospedes</a></li>
    <li role="presentation"><a href="/SRH/reserva/listar">Reservas</a></li>
    <li role="presentation"><a href="#">Checar Disponibilidade</a></li>
    <li role="presentation"><a href="/SRH/logout">Sair</a></li>
  </ul>
</div>
 
<br />
<br />

  <!-- Preenchimento dos campos Categoria -->
  <c:url var="url" value="salvar"/>
  <form:form class="form-horizontal" role="form" action="${url}" method="POST" modelAttribute="categoria">
    <div class="form-group form-group-sm">
      <label class="col-sm-2 control-label" for="sm">Descricao:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="nome" placeholder="Insira a descricao"/>
        <form:errors  path="nome"/>
      </div>
      <label class="col-sm-2 control-label" for="sm">Capacidade:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="capacidade" placeholder="Insira a capacidade"/>
        <form:errors path="capacidade"/>
      </div>
      <label class="col-sm-2 control-label" for="sm">Valor:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="valor" placeholder="Insira o valor"/>
        <form:errors path="valor"/>
      <br />
      <input class="btn btn-default" type="submit" value="Adicionar"/>                     
      </div>
    </div>    
  </form:form>
  


<!-- Modo sem formatacao
	<c:url var="url" value="salvar"/>
	<form:form action="${url}" method="POST" modelAttribute="categoria">
		Nome:<form:input path="nome"/>
		<form:errors path="nome"/>
		<br />
		Capacidade:<form:input path="capacidade"/>
		<form:errors path="capacidade"/>
		<br />
		Valor:<form:input path="valor"/>
		<form:errors path="valor"/>
		<br/>
		<input type="submit" value="salvar"/>
	</form:form>
  -->

</div>
</body>
</html>
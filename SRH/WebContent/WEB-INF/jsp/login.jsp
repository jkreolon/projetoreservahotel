<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repetição e setar variáveis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formatações -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
<script src="bootstrap_v3_3_4/jquery-1.11.3.min.js"> </script>
<script src="bootstrap_v3_3_4/js/bootstrap.js"> </script>
<link href="bootstrap_v3_3_4/css/bootstrap.css" rel="stylesheet">

<meta charset="ISO-8859-1">
<title>Tela de login</title>
</head>
<body>
<div class="container">

  <form:form class="form-signin" method="post" action="efetuarLogin" modelAttribute="usuario"> 
    <h2 class="form-signin-heading">Acesso ao Sistema de Reserva</h2>
    <label class="sr-only">Login</label>
    <form:input path="login" class="form-control" placeholder="Login de acesso"/> <!-- required autofocus -->  
    <label class="sr-only">Senha</label>
    <form:password  path="senha" class="form-control" placeholder="Senha"/> <!-- required -->
    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Logar" />
    <div class="checkbox">   
      <form:errors path="login"/><br />
      <form:errors path="senha"/><br />      
    </div>
  </form:form>
  
  <c:if test="${msg != ''}" > 
    <div class="alert alert-danger" role="alert">
      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
      <span class="sr-only">${msg}</span>
      ${msg}
    </div>
  </c:if>  

</div> <!-- /container -->  


<!-- Login Sem formatacao
  <div>
    ${msg}
  </div>
  <div>
  <form:form method="post" action="efetuarLogin" modelAttribute="usuario">
    <fieldset id="fieldset">
      <legend>Login</legend>
        <div class="LineForm">
          Login:<form:input path="login"/> 
          <form:errors path="login"/><br />
        </div>
        <div>
          Senha:<form:password  path="senha"/> 
          <form:errors path="senha"/><br />
        </div> 
        <div> 
          <input type="submit" value="Logar" />        
        </div>
        </fieldset>
  </form:form>
  </div>
  
   -->

   
</body>
</html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repetição e setar variáveis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formatações -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nova categoria</title>
</head>
<body>
	<c:url var="url" value="salvar"/>
	<form:form action="${url}" method="POST" modelAttribute="categoria">
		Nome:<form:input path="nome"/>
		<form:errors path="nome"/>
		<br />
		Capacidade:<form:input path="capacidade"/>
		<form:errors path="capacidade"/>
		<br />
		Valor:<form:input path="valor"/>
		<form:errors path="valor"/>
		<br/>
		<input type="submit" value="salvar"/>
	</form:form>
  
</body>
</html>
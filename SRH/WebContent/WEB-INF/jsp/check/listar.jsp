<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repeti��o e setar vari�veis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formata��es -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Listagem de Categorias</title>
</head>
<body>

<br />
	<c:url var="url" value="form"/>
	<a href='${url}'>Novo</a>
  <c:url var="url" value="/main"/>	
	 <a href='${url}'>Voltar</a>
	<br />
	<br />
	<table>
	<tr>
	<td>
	C�digo da categoria
	</td>
	<td>
	Nome
	</td>
	<td>
	Capacidade
	</td>
	<td>
	A��es
	</td>
	</tr>
		<c:forEach var="categoria" items="${categorias}">
		<tr>
		<td>
			${categoria.id}
		</td>
		<td>
			${categoria.nome}
		</td>
		<td>
		    ${categoria.capacidade} 
		</td>
		<td>
		
		<c:url var="url" value="/categoria/${categoria.id}/remover"/>
		<a href="${url}">remover</a>
		<c:url var="url" value="/categoria/${categoria.id}/form"/>
		<a href="${url}">editar</a>
		</td>
		</tr>
		</c:forEach>
		
	</table>

</body>
</html>
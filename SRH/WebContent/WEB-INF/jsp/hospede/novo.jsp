<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repeti��o e setar vari�veis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formata��es -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<!DOCTYPE html">
<html>
<head>
<script src="../bootstrap_v3_3_4/jquery-1.11.3.min.js"> </script>
<script src="../bootstrap_v3_3_4/js/bootstrap.js"> </script>
<link href="../bootstrap_v3_3_4/css/bootstrap.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nova Hospede</title>
</head>
<body>
<div class="container">

<!-- Cabe�alho -->
<div class="page-header">
  <h1>Controle de Reservas</h1> 
  <small>
    Bem Vindo: ${usuario.login}
  </small>
</div>

<!-- Main -->
<div>
  <ul class="nav nav-tabs">
    <li role="presentation"><a href="/SRH/categoria/listar">Categorias</a></li>
    <li role="presentation"><a href="/SRH/quarto/listar">Quartos</a></li>
    <li role="presentation"><a href="listar">Hospedes</a></li>
    <li role="presentation"><a href="/SRH/reserva/listar">Reservas</a></li>
    <li role="presentation"><a href="#">Checar Disponibilidade</a></li>
    <li role="presentation"><a href="/SRH/logout">Sair</a></li>
  </ul>
</div> 

<br />
<br />

  <!-- Preenchimento dos campos Quarto -->
  <c:url var="url" value="salvar"/>
  <form:form class="form-horizontal" role="form" action="${url}" method="POST" modelAttribute="hospede">
    <div class="form-group form-group-sm">
      <label class="col-sm-2 control-label" for="sm">Nome:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="nome" placeholder="Insira o nome"/>
        <form:errors  path="nome"/>
      </div>      
      <label class="col-sm-2 control-label" for="sm">CPF:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="CPF" placeholder="Insira o CPF"/>
        <form:errors  path="CPF"/>
      </div>
      <label class="col-sm-2 control-label" for="sm">email:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="email" placeholder="Insira o email"/>
        <form:errors  path="email"/>
      </div>      
      <label class="col-sm-2 control-label" for="sm">Telefone:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="telefone" placeholder="Insira o telefone"/>
        <form:errors  path="telefone"/>
        <br />
      </div>      
      <label class="col-sm-2 control-label" for="sm">Logradouro:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="endereco.longradouro" placeholder="Insira o nome da rua"/>
        <form:errors  path="endereco.longradouro"/>
      </div>      
      <label class="col-sm-2 control-label" for="sm">Bairro:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="endereco.bairro" placeholder="Insira o bairro"/>
        <form:errors  path="endereco.bairro"/>
      </div>      
      <label class="col-sm-2 control-label" for="sm">Numero:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="endereco.numero" placeholder="Insira o n�mero da casa"/>
        <form:errors  path="endereco.numero"/>
      </div>      
      <label class="col-sm-2 control-label" for="sm">CEP:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="endereco.CEP" placeholder="Insira o CEP"/>
        <form:errors  path="endereco.CEP"/>
        <br />
      </div>      
      <label class="col-sm-2 control-label" for="sm">Usuario:</label>
      <div class="col-sm-10">
        <form:input class="form-control" path="usuario.login" placeholder="Insira o usuario"/>
        <form:errors  path="usuario.login"/>
      </div>      
      <label class="col-sm-2 control-label" for="sm">Senha:</label>
      <div class="col-sm-10">
        <form:input class="form-control" type="password" path="usuario.senha" placeholder="Insira a senha"/>
        <form:errors path="usuario.senha"/>
      <br />      
      <input class="btn btn-default" type="submit" value="Adicionar"/>    
      </div>
    </div>    
  </form:form>


<!-- Sem Formatacao
	<c:url var="url" value="/hospede/salvar"/>
	<form:form action="${url}" method="POST" modelAttribute="hospede">
		Nome:<form:input path="nome"/>
		<form:errors path="nome"/>
		<br />
		cpf:<form:input path="CPF"/>
		<form:errors path="CPF"/>
		<br />
		email:<form:input path="email"/>
		<form:errors path="email"/>
		<br />
    telefone:<form:input path="telefone"/>
    <form:errors path="telefone"/>    
    <br />		
    logradouro:<form:input path="endereco.longradouro"/>
    <form:errors path="endereco.longradouro"/>
    <br />    
    bairro:<form:input path="endereco.bairro"/>
    <form:errors path="endereco.bairro"/>
    <br />    
    numero:<form:input path="endereco.numero"/>
    <form:errors path="endereco.numero"/>
    <br />    
    cep:<form:input path="endereco.CEP"/>
    <form:errors path="endereco.CEP"/>
    <br />
    usuario:<form:input path="usuario.login"/>
    <form:errors path="usuario.login"/>
    <br />
    senha:<form:input path="usuario.senha"/>
    <form:errors path="usuario.senha"/>
    <br />            
		<input type="submit" value="salvar"/>
	</form:form>
 -->
 	
</div>  
</body>
</html>
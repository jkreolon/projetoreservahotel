<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repeti��o e setar vari�veis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formata��es -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<!DOCTYPE html>
<html>
<head>
<script src="../bootstrap_v3_3_4/jquery-1.11.3.min.js"> </script>
<script src="../bootstrap_v3_3_4/js/bootstrap.js"> </script>
<link href="../bootstrap_v3_3_4/css/bootstrap.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Listagem de Hospedes</title>
</head>
<body>
<div class="container">

<!-- Cabe�alho -->
<div class="page-header">
  <h1>Controle de Reservas</h1> 
  <small>
    Bem Vindo: ${usuario.login}
  </small>
</div>

<!-- Main -->
<div>
  <ul class="nav nav-tabs">
    <li role="presentation"><a href="/SRH/categoria/listar">Categorias</a></li>
    <li role="presentation"><a href="/SRH/quarto/listar">Quartos</a></li>
    <li role="presentation"><a href="listar">Hospedes</a></li>
    <li role="presentation"><a href="/SRH/reserva/listar">Reservas</a></li>
    <li role="presentation"><a href="#">Checar Disponibilidade</a></li>
    <li role="presentation"><a href="/SRH/logout">Sair</a></li>
  </ul>
</div> 

<br />
<br />

<!-- Listagem da Hospedes -->
    <form:form class="form-inline" action="${urlFiltro}" method="GET" modelAttribute="filtro">
      <div class="form-group">
        <label>Nome: </label>
        <form:input class="form-control" path="nome" placeholder="Insira o nome"/>
        <button type="submit" class="btn btn-default">Pesquisar</button>
      </div>
    </form:form>     
    <br />
    <br />
   <c:url var="urlFiltro" value="/hospede/pesquisalike" />
    <form:form class="form-inline" action="${urlFiltro}" method="GET" modelAttribute="filtro">
    <div class="form-group">
      <label>CPF...: </label>
      <form:input class="form-control" path="CPF" placeholder="Insira o CPF"/>
      <button type="submit" class="btn btn-default">Pesquisar</button>
    </div>
  </form:form>
<br />
<br />

<div class="btn-group" role="group" aria-label="...">
  <form:form method="get" action="form">
    <button type="submit" class="btn btn-default" >Novo</button>
  </form:form>  
</div>

  <div class="table-responsive">
    <table class="table">
      <tr>
        <td>Codigo</td>
        <td>Nome</td>
        <td>CPF</td>
        <td>email</td>
        <td>Contato</td>
        <td>Acoes</td>
        <td></td>
      </tr>
      <c:forEach var="hospede" items="${hospedes}">  
      <tr>
        <td>${hospede.id}</td>
        <td>${hospede.nome}</td>
        <td>${hospede.CPF}</td>
        <td>${hospede.email} </td>
        <td>${hospede.telefone} </td>
        <td>
          <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              Opcao <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <c:url var="url" value="/hospede/${hospede.id}/remover"/>
              <li><a href="${url}">Remover</a></li>
              <c:url var="url" value="/hospede/${hospede.id}/formDetail"/>
              <li><a href="${url}">Detalhar</a></li>              
              <c:url var="url" value="/hospede/${hospede.id}/form"/>
              <li><a href="${url}">Editar</a></li>              
            </ul>
          </div>    
        </td>
      </tr>
      </c:forEach> 
    </table>
  </div>


<!-- Sem formatacao
	<c:url var="url" value="/hospede/form"/>
	<a href='${url}'>Novo</a>
  <c:url var="url" value="/main"/>  
  <a href='${url}'>Voltar</a>
	<br />
	<br />
	<table>
	<tr>
	<td>
	C�digo
	</td>
	<td>
	Nome
	</td>
	<td>
	CPF
	</td>
  <td>
  email
  </td>
  <td>
  Contato
  </td>    
	<td>
	A��es
	</td>
	</tr>
		<c:forEach var="hospede" items="${hospedes}">
		<tr>
		<td>
			${hospede.id}
		</td>
		<td>
			${hospede.nome}
		</td>
		<td>
		  ${hospede.CPF} 
		</td>
    <td>
      ${hospede.email} 
    </td>		
		<td>
		<c:url var="url" value="/hospede/${hospede.id}/remover"/>
		<a href="${url}">remover</a>
		<c:url var="url" value="/categoria/${hospede.id}/form"/>
		<a href="${url}">editar</a>
		</td>
		</tr>
		</c:forEach>
	</table>
 -->
</div>
</body>
</html>
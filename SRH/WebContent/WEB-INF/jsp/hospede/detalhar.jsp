<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repeti��o e setar vari�veis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formata��es -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<!DOCTYPE html">
<html>
<head>
<script src="../../bootstrap_v3_3_4/jquery-1.11.3.min.js"> </script>
<script src="../../bootstrap_v3_3_4/js/bootstrap.js"> </script>
<link href="../../bootstrap_v3_3_4/css/bootstrap.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detalhe do Hospede</title>
</head>
<body>
<div class="container">

<!-- Cabe�alho -->
<div class="page-header">
  <h1>Controle de Reservas</h1> 
  <small>
    Bem Vindo: ${usuario.login}
  </small>
</div>

<!-- Main -->
<div>
  <ul class="nav nav-tabs">
    <li role="presentation"><a href="/SRH/categoria/listar">Categorias</a></li>
    <li role="presentation"><a href="/SRH/quarto/listar">Quartos</a></li>
    <li role="presentation"><a href="listar">Hospedes</a></li>
    <li role="presentation"><a href="#">Reservas</a></li>
    <li role="presentation"><a href="#">Checar Disponibilidade</a></li>
    <li role="presentation"><a href="/SRH/logout">Sair</a></li>
  </ul>
</div> 

<br />
<br />

  <div class="table-responsive">
    <table class="table">
      <tr>
        <td>Codigo</td>
        <td>${id}</td>
      </tr>
      <tr>  
        <td>Nome</td>
        <td>${hospede.nome}</td>
      </tr>  
      <tr>
        <td>CPF</td>
        <td>${hospede.CPF}</td>
      </tr>
      <tr>
        <td>email</td>
        <td>${hospede.email} </td>
      </tr>
      <tr>
        <td>Contato</td>
        <td>${hospede.telefone} </td>
      </tr>
      <tr>
        <td>Logradouro</td>
        <td>${hospede.endereco.longradouro}</td>
      </tr>
      <tr>
        <td>Bairro</td>
        <td>${hospede.endereco.bairro}</td>
      </tr>        
      <tr>
        <td>Numero</td>
        <td>${hospede.endereco.numero}</td>
      </tr>
      <tr>
        <td>CEP</td>
        <td>${hospede.endereco.CEP}</td>
      </tr>
      <tr>
        <td>Login</td>
        <td>${hospede.usuario.login}</td>
      </tr> 
    </table>
  </div>
 
</div>
</body>
</html>

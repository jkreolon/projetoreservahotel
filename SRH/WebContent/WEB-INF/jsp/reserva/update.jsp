<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repeti��o e setar vari�veis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formata��es -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<!DOCTYPE html">
<html>
<head>
<script src="../../bootstrap_v3_3_4/jquery-1.11.3.min.js"> </script>
<script src="../../bootstrap_v3_3_4/js/bootstrap.js"> </script>
<link href="../../bootstrap_v3_3_4/css/bootstrap.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar Reserva</title>
</head>
<body>
<div class="container">

<!-- Cabe�alho -->
<div class="page-header">
  <h1>Controle de Reservas</h1> 
  <small>
    Bem Vindo: ${usuario.login}
  </small>
</div>

<!-- Main -->
<div>
  <ul class="nav nav-tabs">
    <li role="presentation"><a href="/SRH/categoria/listar">Categorias</a></li>
    <li role="presentation"><a href="listar">Quartos</a></li>
    <li role="presentation"><a href="/SRH/hospede/listar">Hospedes</a></li>
    <li role="presentation"><a href="listar">Reservas</a></li>
    <li role="presentation"><a href="#">Checar Disponibilidade</a></li>
    <li role="presentation"><a href="/SRH/logout">Sair</a></li>
  </ul>
</div> 

<br />
<br />

  <!-- Preenchimento dos campos Reserva -->
  <c:url var="url" value="/reserva/update"/>
  <form:form class="form-horizontal" role="form" action="${url}" method="POST" modelAttribute="reserva">
    <div class="form-group form-group-sm">
      <form:hidden path="id"/>      
      <label class="col-sm-2 control-label" for="sm">Data Inicio:</label>
      <div class="col-sm-10">
        <form:input  class="form-control" path="dataInicial" placeholder="Insira a data de inicio"/>
        <form:errors  path="dataInicial"/>
      </div>
      <label class="col-sm-2 control-label" for="sm">Data Fim:</label>
      <div class="col-sm-10">
        <form:input  class="form-control" path="dataFinal" placeholder="Insira a data de Fim"/>
        <form:errors  path="dataFinal"/>
      </div>              
      <label class="col-sm-2 control-label" for="sm">Cliente:</label>
      <div class="col-sm-10">
        <form:select class="col-sm-2 control-label" path="hospede.id" items="${selectHospede}"/>
        <form:errors  path="hospede.id"/>
        <br />
      </div>      
      <label class="col-sm-2 control-label" for="sm">Quarto:</label>
      <div class="col-sm-10">
        <form:select class="col-sm-2 control-label" path="quarto.id" items="${selectQuarto}"/>
        <form:errors  path="quarto.id"/>
        <br />
      </div>
      <div class="col-sm-10">
      <br />      
      <input class="btn btn-default" type="submit" value="Atualizar"/>    
      </div>
    </div>    
  </form:form>


</div>  
</body>
</html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- para estruturas de controle e repeti��o e setar vari�veis -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %> <!-- para formata��es -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html">
<html>
<head>
<script src="../bootstrap_v3_3_4/jquery-1.11.3.min.js"> </script>
<script src="../bootstrap_v3_3_4/js/bootstrap.js"> </script>
<link href="../bootstrap_v3_3_4/css/bootstrap.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Listagem de Reservas</title>
</head>
<body>
<div class="container">

<!-- Cabe�alho -->
<div class="page-header">
  <h1>Controle de Reservas</h1> 
  <small>
    Bem Vindo: ${usuario.login}
  </small>
</div>

<!-- Main -->
<div>
  <ul class="nav nav-tabs">
    <li role="presentation"><a href="/SRH/categoria/listar">Categorias</a></li>
    <li role="presentation"><a href="/SRH/quarto/listar">Quartos</a></li>
    <li role="presentation"><a href="/SRH/hospede/listar">Hospedes</a></li>
    <li role="presentation"><a href="listar">Reservas</a></li>
    <li role="presentation"><a href="#">Checar Disponibilidade</a></li>
    <li role="presentation"><a href="/SRH/logout">Sair</a></li>
  </ul>
</div> 

<br />
${msg}
<br />

<div class="panel panel-default">
  <div class="panel-heading">Pesquisar</div>
  <div class="panel-body">
    <form class="form-inline"  method="GET">
      <div class="form-group">
        <label>Data inicio: </label>
        <input class="form-control"placeholder="Insira a data inicial"/>
        <label>Data fim: </label>
        <input class="form-control"placeholder="Insira a data final "/>
        <label>Status: </label>
        <select class="form-control"/>
          <option value="Confirmado">Confirmado</option>
          <option value="Cancelado">Cancelado</option>
          <option value="Pendente" selected="selected">Pendente</option>
        </select>   
        <br />            
        <br />
    
        <button type="submit" class="btn btn-default">Pesquisar</button>
      </div>
    </form>  
  </div>
</div>

<br />
<br />

<div class="btn-group" role="group" aria-label="...">
  <form:form method="get" action="form">
    <button type="submit" class="btn btn-default" >Novo</button>
  </form:form>  
</div>

  <div class="table-responsive">
    <table class="table">
      <tr>
        <td>Numero</td>
        <td>Data Inicio</td>
        <td>Data Fim</td>
        <td>Data Checkin</td>
        <td>Data Checkout</td>
        <td>Cliente</td>
        <td>Quarto</td>
        <td>Status</td>
        <td>Pago</td>
      </tr>
      <c:forEach var="reserva" items="${reservas}">  
      <tr>
        <td>${reserva.id}</td>
        <td><fmt:formatDate pattern="dd-MM-yyyy" value="${reserva.dataInicial}"/></td>
        <td><fmt:formatDate pattern="dd-MM-yyyy" value="${reserva.dataFinal}"/></td>
        <td><fmt:formatDate pattern="dd-MM-yyyy" value="${reserva.dataCheckin}"/></td>
        <td><fmt:formatDate pattern="dd-MM-yyyy" value="${reserva.dataCheckout}"/></td>
        <td>${reserva.hospede.nome}</td>
        <td>${reserva.quarto.numero}</td>
        <td>${reserva.status}</td>
        <td>${reserva.isPago}</td>
        <td>
          <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              Opcao <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <c:url var="url" value="/reserva/${reserva.id}/remover"/>
              <li><a href="${url}">Remover</a></li>
              <c:url var="url" value="/reserva/${reserva.id}/form"/>
              <li><a href="${url}">Editar</a></li>
            </ul>
          </div>    
        </td>
      </tr>
      </c:forEach> 
    </table>
  </div>
</div>

<!-- 
<br />
  <c:url var="url" value="/quarto/form"/>
  <a href='${url}'>Novo</a>
  <c:url var="url" value="/main"/>  
  <a href='${url}'>Voltar</a>
  <br />
  <br />
  <table>
   <tr>
     <td>
       C�d Quarto
     </td>
     <td>
       Andar
     </td>
     <td>
       Numero
     </td>
     <td>
       A��es
     </td>
  </tr>
   <c:forEach var="quarto" items="${quartos}">
  <tr>
   <td>
     ${quarto.id}
   </td>
   <td>
     ${quarto.andar}
   </td>
   <td>
     ${quarto.numero} 
   </td>
   <td>
     ${quarto.categoria.nome} 
   </td>    
   <td>
     <c:url var="url" value="/quarto/${quarto.id}/remover"/>
     <a href="${url}">remover</a>
     <c:url var="url" value="/quarto/${quarto.id}/form"/>
     <a href="${url}">editar</a>
   </td>
  </tr>
    </c:forEach>
  </table>
   -->
 
</div>
</body>
</html>

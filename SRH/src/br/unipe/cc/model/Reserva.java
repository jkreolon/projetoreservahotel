package br.unipe.cc.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@SequenceGenerator(name = "reserva_id",sequenceName = "seq_reserva",allocationSize=1)
public class Reserva extends AbstractEntity {
	
	public enum FormaPagamento {
		dinheiro, cartaoCredito, cartaoDebito;
	}

	public enum Status {
		Pending {
			public String toString(){
				return "PE";
			}
		}, 
		Canceled{
			public String toString(){
				return "CA";
			}
		}, 
		Confirmed{
			public String toString(){
				return "CO";
			}
		},
		free{
			public String toString(){
				return "free";
			}
		};
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="reserva_id")
	private long id;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataInicial;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataFinal;
	
	@Temporal(TemporalType.DATE)
	private Date dataCheckin;
	
	@Temporal(TemporalType.DATE)
	private Date dataCheckout;
	
	private boolean isPago;
	
	@Enumerated(EnumType.STRING)
	private String formaPagamento;
	
	@Enumerated(EnumType.STRING)
	private String status;
	
	private float valorPago;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Hospede hospede;

	@ManyToOne
	private Funcionario funcionario;

	@ManyToOne
	private Quarto quarto;

	@OneToMany(mappedBy = "reserva")
	private List<Consumo> consumos;
	
	public Reserva() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Reserva(long id) {
		super();
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Date getDataCheckin() {
		return dataCheckin;
	}

	public void setDataCheckin(Date dataCheckin) {
		this.dataCheckin = dataCheckin;
	}

	public Date getDataCheckout() {
		return dataCheckout;
	}

	public void setDataCheckout(Date dataCheckout) {
		this.dataCheckout = dataCheckout;
	}
	
	public boolean getIsPago() {
		return isPago;
	}

	public void setPago(boolean isPago) {
		this.isPago = isPago;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public float getValorPago() {
		return valorPago;
	}

	public void setValorPago(float valorPago) {
		this.valorPago = valorPago;
	}

	public Quarto getQuarto() {
		return quarto;
	}

	public void setQuarto(Quarto quarto) {
		this.quarto = quarto;
	}

	public List<Consumo> getConsumos() {
		return consumos;
	}

	public void setConsumos(List<Consumo> consumos) {
		this.consumos = consumos;
	}

	public Hospede getHospede() {
		return hospede;
	}

	public void setHospede(Hospede hospede) {
		this.hospede = hospede;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
}

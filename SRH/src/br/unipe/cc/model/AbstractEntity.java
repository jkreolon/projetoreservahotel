package br.unipe.cc.model;

public abstract class AbstractEntity {
	
	public abstract long getId();
	public abstract void setId(long id);
	
	public boolean hasValidID(){
		return getId() == 0 ;
	}
	
	public boolean equals(Object object){
		AbstractEntity entity = (AbstractEntity) object;
		return entity.getId() == this.getId();
	}
}


package br.unipe.cc.model.DAO;
import java.util.List;

import javax.persistence.NamedQuery;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.unipe.cc.model.Usuario;

/**
 * 
 * @author JimRol
 * @Definition
 *	Classe para manipulacao dos m�todos de persist�ncia e controle (Selects)
 */

@Repository
@NamedQuery(name = "Usuario.findID",query = "SELECT u from Usuario u where u.login = :login")
public class UsuarioDAO extends AbstractDAO<Usuario>{
	
	
	public Usuario buscarID(Usuario usuario){
		Query query = manager.createNamedQuery("SELECT u from Usuario u where u.login = :login");
		query.setParameter("login",usuario.getLogin());
		return (Usuario) query.getSingleResult();
		
	}	
	
	
	@Override
	public Class<Usuario> entityClass(){
		return Usuario.class;
	}

	public Usuario efetuarLogin(String login,String senha){
		Query query = manager.createQuery("select u from Usuario u where u.login = :login and u.senha = :senha");
		query.setParameter("login", login);
		query.setParameter("senha", senha);
		List<Usuario> usuarios = query.getResultList();
		if (usuarios != null && !usuarios.isEmpty()){
			return usuarios.get(0);
		}
		return null;
	}
}

package br.unipe.cc.model.DAO;

import org.springframework.stereotype.Repository;

import br.unipe.cc.model.Pessoa;

/**
 * 
 * @author JimRol
 * @Definition
 *	Classe para manipulacao dos m�todos de persist�ncia e controle (Selects)
 */

@Repository
public class PessoaDAO extends AbstractDAO<Pessoa> {

	@Override
	public Class<Pessoa> entityClass(){
		return Pessoa.class;
	}
	
}
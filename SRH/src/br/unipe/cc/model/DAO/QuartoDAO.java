package br.unipe.cc.model.DAO;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.Quarto;

/**
 * 
 * @author JimRol
 * @Definition
 *	Classe para manipulacao dos m�todos de persist�ncia e controle (Selects)
 */

@Repository
public class QuartoDAO extends AbstractDAO<Quarto> {
	
	@Override
	public Class<Quarto> entityClass(){
		return Quarto.class;
	}
	
	public List<Quarto> listarQuartoDisponivel(String dataInicio, String dataFim){
				
		String querySQL = "select" 
				  + " q.id,"
				  + " q.andar,"
				  + " q.numero,"
				  + " q.categoria_id"
				  + " from"
				  + "  quarto q"
				  + " where" 
				  + "   q.id not in ("
				  + " select r.quarto_id from"   
				  + "    ("
				  + "     select" 
				  + "       q.id quarto_id,"
				  + "       r.id,"
				  + "       r.datainicial,"
				  + "       r.datafinal,"
				  + "       r.status,"
				  + "       q.andar,"
				  + "       q.numero"
				  + "     from" 
				  + "       reserva r"
				  + "       join quarto q on (r.quarto_id = q.id)"
				  + "     where" 
				  + "       (r.dataInicial between (select MIN(dataInicial) from reserva) and '2015-03-23' ) or (r.dataFinal between '2015-03-26' and (select MAX(dataFinal) from reserva))"
				  + "     ) as tb join reserva r on (tb.id = r.id)"
				  + " where"
				  + "   r.status in ('PE','CO'))";    
		Query query = manager.createNativeQuery(querySQL);
		return query.getResultList();
	}

	public List<Quarto> filtrar(Quarto quarto){
		/*Query query = manager.createNamedQuery("Categoria.haveCategoria");*/
		String querySQL = "select quarto from Quarto quarto where quarto.andar = :andar ";
		Query query = manager.createQuery(querySQL);
		query.setParameter("andar",quarto.getAndar());
		return  query.getResultList();
		
	}	
	
	
}

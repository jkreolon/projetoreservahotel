package br.unipe.cc.model.DAO;
import java.text.SimpleDateFormat;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.unipe.cc.model.Reserva;

/**
 * 
 * @author JimRol
 * @Definition
 *	Classe para manipulacao dos m�todos de persist�ncia e controle (Selects)
 */

@Repository
public class ReservaDAO extends AbstractDAO<Reserva> {

	
	@Override
	public Class<Reserva> entityClass(){
		return Reserva.class;
	}
	
	public int alterStatus(Reserva reserva){
		String querySQL = "update Reserva reserva set reserva.status = '"+ reserva.getStatus() +
				          "' where reserva.id = " + reserva.getId();
		Query query = manager.createQuery(querySQL);
		int effectLines = query.executeUpdate();
		return effectLines;
	}
	
	public int alterCheck(Reserva reserva){
		SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
		String data = sp.format(reserva.getDataCheckin());
		
		String querySQL;
		if (reserva.getDataCheckin() != null){
			querySQL = "update Reserva reserva set reserva.dataCheckin = '"+ data +"' where reserva.id = " + reserva.getId();  
		}else{
			querySQL = "update Reserva reserva set reserva.dataCheckout = " + reserva.getDataCheckout() + " where reserva.id = " + reserva.getId(); 
		} 
		Query query = manager.createQuery(querySQL);		
		int effectLines = query.executeUpdate();
		return effectLines;
	}

	public int novoPagamento(Reserva reserva){
		String querySQL = "update Reserva reserva set reserva.ispago = '"+ reserva.getIsPago() +
				          "' where reserva.id = " + reserva.getId();
		Query query = manager.createQuery(querySQL);
		int effectLines = query.executeUpdate();
		return effectLines;
	}
}
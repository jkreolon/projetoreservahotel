package br.unipe.cc.model.DAO;

import org.springframework.stereotype.Repository;
import br.unipe.cc.model.Consumo;

/**
 * 
 * @author JimRol
 * @Definition
 *	Classe para manipulacao dos m�todos de persist�ncia e controle (Selects)
 */

@Repository
public class ConsumoDAO extends AbstractDAO<Consumo> {

	@Override
	public Class<Consumo> entityClass(){
		return Consumo.class;
	}
	
}

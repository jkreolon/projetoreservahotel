package br.unipe.cc.model.DAO;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import br.unipe.cc.model.Produto;

/**
 * 
 * @author JimRol
 * @Definition
 *	Classe para manipulacao dos m�todos de persist�ncia e controle (Selects) 
 */

@Repository
public class ProdutoDAO extends AbstractDAO<Produto> {

	
	@Override
	public Class<Produto> entityClass(){
		return Produto.class;
	}

	/**
	 * 
	 * @param produto
	 * @return
	 */
	public int haveCategoria(Produto produto){
		String querySQL = "select count(*) from Consumo consumo where produto_id = :id";
		TypedQuery<Long> query = manager.createQuery(querySQL,Long.class);
		query.setParameter("id",produto.getId());
		Long value = query.getSingleResult();
		return  value.intValue();
		
	}	
	
}
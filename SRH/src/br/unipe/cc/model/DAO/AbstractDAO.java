package br.unipe.cc.model.DAO;

import java.util.List;

import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.EntityManager;

import br.unipe.cc.model.AbstractEntity;

/**
 * 
 * @author JimRol
 * @Definition
 * Classe abstrata contendo os m�todos comuns �s classes DAO
 * @param <T>
 */

public abstract class AbstractDAO<T extends AbstractEntity> {
	
	@PersistenceContext
	protected EntityManager manager;

	
	public void salvar(T entity){
		manager.persist(entity);
	}	
	
	public void atualizar(T entity){
		manager.merge(entity);
	}
	
	public void remover(T entity){
		entity = manager.find(entityClass(), entity.getId());
		manager.remove(entity);
	}
	
	public List<T> listar(){
		Query query = manager.createQuery("select c from " + entityClass().getSimpleName() + " c");
		return query.getResultList();
	}	

	public T buscarPorId(long id){
		return manager.find(entityClass(), id);
	}
	
	public abstract Class<T> entityClass();
}

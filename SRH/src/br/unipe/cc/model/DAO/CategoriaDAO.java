package br.unipe.cc.model.DAO;

import java.util.List;

import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.unipe.cc.model.Categoria;

/**
 * 
 * @author JimRol
 * @Definition
 *	Classe para manipulacao dos m�todos de persist�ncia e controle (Selects) 
 */

@Repository
@NamedQuery(name="Categoria.haveCategoria",query = "select count(q.id) from Quarto q where q.categoria.id = :id")
public class CategoriaDAO extends AbstractDAO<Categoria>{
	
	
	@Override
	public Class<Categoria> entityClass(){
		return Categoria.class;
	}
	
	/*todos JPQL entram no DAO
	select c from Categoria c*/
	/**
	 * 
	 * @param categoria
	 * @return
	 */
	public int haveCategoria(Categoria categoria){
		/*Query query = manager.createNamedQuery("Categoria.haveCategoria");*/
		String querySQL = "select count(*) from Quarto q where q.categoria.id = :id";
		TypedQuery <Object> query = manager.createQuery(querySQL,Object.class);
		query.setParameter("id",categoria.getId());
		Object singleResult = query.getSingleResult();
		return  ((Number) singleResult).intValue();
		
	}

	public List<Categoria> filtrar(Categoria categoria){
		/*Query query = manager.createNamedQuery("Categoria.haveCategoria");*/
		String querySQL = "select cat from Categoria cat where upper(cat.nome) like upper(:nome)";
		Query query = manager.createQuery(querySQL);
		query.setParameter("nome","%"+categoria.getNome()+"%");
		return  query.getResultList();
		
	}
	
}

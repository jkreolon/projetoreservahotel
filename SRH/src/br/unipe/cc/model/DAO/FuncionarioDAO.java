package br.unipe.cc.model.DAO;

import org.springframework.stereotype.Repository;
import br.unipe.cc.model.Funcionario;

/**
 * 
 * @author JimRol
 * @Definition
 *	Classe para manipulacao dos m�todos de persist�ncia e controle (Selects)
 */

@Repository
public class FuncionarioDAO extends AbstractDAO<Funcionario> {

	@Override
	public Class<Funcionario> entityClass(){
		return Funcionario.class;
	}
	
}
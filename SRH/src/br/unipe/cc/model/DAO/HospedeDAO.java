package br.unipe.cc.model.DAO;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.Hospede;

/**
 * 
 * @author JimRol
 * @Definition
 *	Classe para manipulacao dos m�todos de persist�ncia e controle (Selects)
 */

@Repository
public class HospedeDAO extends AbstractDAO<Hospede>{
	
	
	@Override
	public Class<Hospede> entityClass(){
		return Hospede.class;
	}
	
	public List<Hospede> listarHospede(Hospede hospede){
		String querySQL = 
			"	select "
			+"	pessoa "
			+"	from "
			+"	 Pessoa pessoa "
			+"	 join hospede h on (p.id = h.id ) "
			+"	 join reserva r on (p.id = r.hospede_id) "
			+"	where "
			+"	 r.hospede_id = " + hospede.getId() +" "
			+"	order by "
			+"	 r.datainicial ";
		Query query = manager.createQuery(querySQL);
		return  query.getResultList();
	}

	public List<Hospede> filtrar(Hospede hospede){
		/*Query query = manager.createNamedQuery("Categoria.haveCategoria");*/
		if ((hospede.getNome() != null) && (hospede.getCPF() == null)){
			String querySQL = "select hospede from Hospede hospede where upper(hospede.nome) like upper(:nome)";
			Query query = manager.createQuery(querySQL);
			query.setParameter("nome","%"+hospede.getNome()+"%");
			return  query.getResultList();
		}else
			if ((hospede.getNome() == null) && (hospede.getCPF() != null)){
			String querySQL = "select hospede from Hospede hospede where hospede.CPF like :cpf";
			Query query = manager.createQuery(querySQL);
			query.setParameter("cpf","%"+hospede.getCPF()+"%");
			return  query.getResultList();
		}
		String querySQL = "select hospede from Hospede hospede";
		Query query = manager.createQuery(querySQL);		
		return  query.getResultList();
		
	}	
}

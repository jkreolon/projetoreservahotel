package br.unipe.cc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;


@Entity
@SequenceGenerator(name = "quarto_id",sequenceName="seq_quarto",allocationSize=1)
public class Quarto extends AbstractEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="quarto_id")
	private long id;
	
	@NotNull(message="O numero n�o pode ser vazio")	
	private Integer numero;
	
	@NotNull(message="O andar n�o pode ser vazio")
	private Integer andar;
	
	@ManyToOne
	private Categoria categoria;
	
	public Quarto() {
		super();
	}

	public Quarto(long id) {
		super();
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getAndar() {
		return andar;
	}

	public void setAndar(Integer andar) {
		this.andar = andar;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
}

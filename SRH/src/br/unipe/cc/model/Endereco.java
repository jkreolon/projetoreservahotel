package br.unipe.cc.model;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Embeddable
public class Endereco  {
	
	@NotEmpty(message="O endereco n�o pode ser vazio")
	private String longradouro;
	
	@NotEmpty(message="A cidade n�o pode ser vazio")
	private String cidade;
	
	@NotEmpty(message="O bairro n�o pode ser vazio")
	private String bairro;
	
	@NotEmpty(message="O CEP n�o pode ser vazio")
	private String CEP;
	
	@NotNull(message="O numero n�o pode se vazio")
	private Integer numero;
	
	public Endereco() {
		super();
	}

	public String getLongradouro() {
		return longradouro;
	}

	public void setLongradouro(String longradouro) {
		this.longradouro = longradouro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCEP() {
		return CEP;
	}

	public void setCEP(String CEP) {
		this.CEP = CEP;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}
}

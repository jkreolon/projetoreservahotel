package br.unipe.cc.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.DAO.CategoriaDAO;

@Service
@Transactional
public class CategoriaService {
	
	@Autowired
	CategoriaDAO categoriaDAO;
	
	public void salvar(Categoria categoria){
		categoriaDAO.salvar(categoria);
	}
	
	public List<Categoria> listar(){
		return categoriaDAO.listar();
	}

	public List<Categoria> filtrar(Categoria categoria){
		return categoriaDAO.filtrar(categoria);
	}	

	public void atualizar(Categoria categoria){
		categoriaDAO.atualizar(categoria);		
	}
	
	public void remover(Categoria categoria){
		if (categoriaDAO.haveCategoria(categoria) > 0) {
			throw new RuntimeException("Objeto possui dependencia com Quarto");
		}		
		categoriaDAO.remover(categoria);
	}	
				
	public Categoria buscarPorId(long id){
		return categoriaDAO.buscarPorId(id);
	}

}

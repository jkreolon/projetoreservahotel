package br.unipe.cc.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.unipe.cc.model.Usuario;
import br.unipe.cc.model.DAO.UsuarioDAO;

@Service
@Transactional
public class UsuarioService {

	@Autowired
	UsuarioDAO usuarioDAO;
	
	public void salvar(Usuario usuario){
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		usuarioDAO.salvar(usuario);
	}
	
	public List<Usuario> listar(){
		return usuarioDAO.listar();
	}
	public void atualizar(Usuario usuario){
		usuarioDAO.atualizar(usuario);
	}
	
	public void remover(Usuario usuario){
		usuarioDAO.remover(usuario);
	}	
					
	public Usuario buscarPorId(long id){
		return usuarioDAO.buscarPorId(id);
	}	

	public Usuario efetuarLogin(String login,String senha){
		return usuarioDAO.efetuarLogin(login,senha);
	}
}

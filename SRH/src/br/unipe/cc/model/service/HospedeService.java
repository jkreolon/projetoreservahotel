package br.unipe.cc.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.Hospede;
import br.unipe.cc.model.DAO.HospedeDAO;
import br.unipe.cc.model.DAO.UsuarioDAO;

@Service
@Transactional
public class HospedeService extends HospedeDAO{
	
	@Autowired
	private HospedeDAO hospedeDAO;
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	public void salvar(Hospede hospede){
		
		if(hospede.getUsuario() == null){
			throw new RuntimeException("Usuario nao cadastrado");
		}else
			if (hospede.getUsuario().hasValidID()){
				usuarioDAO.salvar(hospede.getUsuario());
			}		
		hospedeDAO.salvar(hospede);
	}
	
	public void atualizar(Hospede hospede){
		
		if(hospede.getUsuario() == null || hospede.getUsuario().hasValidID()){
			throw new RuntimeException("Usuario nao cadastrado");
		}
		hospedeDAO.atualizar(hospede);
	}
	
	public void remover(Hospede hospede){
		hospedeDAO.remover(hospede);
		
	}
	

	public List<Hospede> listar(){
		return hospedeDAO.listar();
	}

	public List<Hospede> filtrar(Hospede hospede){
		return hospedeDAO.filtrar(hospede);
	}
		
}

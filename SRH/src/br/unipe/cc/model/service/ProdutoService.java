package br.unipe.cc.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.unipe.cc.model.Produto;
import br.unipe.cc.model.DAO.ProdutoDAO;

@Service
@Transactional
public class ProdutoService extends ProdutoDAO{

	@Autowired
	ProdutoDAO produtoDAO;
	
	public void salvar(Produto Produto){
		ProdutoDAO ProdutoDAO = new ProdutoDAO();
		ProdutoDAO.salvar(Produto);
	}

	public void atualizar(Produto Produto){
		ProdutoDAO ProdutoDAO = new ProdutoDAO();
		ProdutoDAO.atualizar(Produto);
	}	
	
	public List<Produto> listar(){
		ProdutoDAO produtoDAO = new ProdutoDAO();
		return produtoDAO.listar();
	}
	
	public void remover(Produto produto){
		ProdutoDAO produtoDAO = new ProdutoDAO();
		if (produtoDAO.haveCategoria(produto) > 0) {
			throw new RuntimeException("Objeto possui dependencia com Consumo");
		}		
		produtoDAO.remover(produto);
	}	

	
}

package br.unipe.cc.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.unipe.cc.model.Funcionario;
import br.unipe.cc.model.DAO.FuncionarioDAO;
import br.unipe.cc.model.DAO.UsuarioDAO;

@Service
@Transactional
public class FuncionarioService extends FuncionarioDAO {

	@Autowired
	FuncionarioDAO funcionarioDAO;
	
	@Autowired
	UsuarioDAO usuarioDAO;
	
	public void salvar(Funcionario funcionario){
		FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
		
		if(funcionario.getUsuario() == null){
			throw new RuntimeException("Usuario nao cadastrado");
		}else
			if (funcionario.getUsuario().hasValidID()){
				UsuarioDAO usuarioDAO = new UsuarioDAO();
				usuarioDAO.salvar(funcionario.getUsuario());
			}
		funcionarioDAO.salvar(funcionario);
	}
	
	public void atualizar(Funcionario funcionario){
		FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
		
		if(funcionario.getUsuario() == null || funcionario.getUsuario().hasValidID()){
			throw new RuntimeException("Usuario nao cadastrado");
		}		
		funcionarioDAO.atualizar(funcionario);		
	}
	
	public void remover(Funcionario funcionario){
		FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
		funcionarioDAO.remover(funcionario);		
	}
	

	public List<Funcionario> listar(){
		FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
		return funcionarioDAO.listar();
	}	
}

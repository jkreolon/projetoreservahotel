package br.unipe.cc.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.unipe.cc.model.Pessoa;
import br.unipe.cc.model.DAO.PessoaDAO;
import br.unipe.cc.model.DAO.UsuarioDAO;


@Service
@Transactional
public class PessoaService extends PessoaDAO{
	
	@Autowired
	PessoaDAO pessoaDAO;
	
	@Autowired
	UsuarioDAO usuarioDAO;
	
	public void salvar(Pessoa pessoa){
		PessoaDAO pessoaDAO = new PessoaDAO();
		
		if(pessoa.getUsuario() == null){
			throw new RuntimeException("Usuario nao cadastrado");
		}else
			if (pessoa.getUsuario().hasValidID()){
				usuarioDAO.salvar(pessoa.getUsuario());
			}		
		pessoaDAO.salvar(pessoa);
	}
	
	public List<Pessoa> listar(){
		return pessoaDAO.listar();
	}

	public void atualizar(Pessoa pessoa){
		
		if(pessoa.getUsuario() == null || pessoa.getUsuario().hasValidID()){
			throw new RuntimeException("Usuario nao cadastrado");
		}		
		pessoaDAO.atualizar(pessoa);		
	}	
	
	public void remover(Pessoa pessoa){
		pessoaDAO.remover(pessoa);
	}
	
}

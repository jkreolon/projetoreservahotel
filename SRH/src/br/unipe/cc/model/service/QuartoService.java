package br.unipe.cc.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.Quarto;
import br.unipe.cc.model.DAO.QuartoDAO;

@Service
@Transactional
public class QuartoService extends QuartoDAO{

	@Autowired
	QuartoDAO quartoDAO;
	
	
	public void salvar(Quarto quarto){
		
		if(quarto.getCategoria() == null || quarto.getCategoria().hasValidID()){
			throw new RuntimeException("Quarto sem categoria");
		}
		quartoDAO.salvar(quarto);
	}
	
	public List<Quarto> listar(){
		return quartoDAO.listar();
	}

	public void atualizar(Quarto quarto){
		
		if(quarto.getCategoria() == null || quarto.getCategoria().hasValidID()){
			throw new RuntimeException("Quarto sem categoria");
		}		
		quartoDAO.atualizar(quarto);			
	}	
	
	public void remover(Quarto quarto){
		quartoDAO.remover(quarto);
	}
	
	public List<Quarto> listarQuartosDisponiveis(){
		return quartoDAO.listarQuartoDisponivel();
	}

	public List<Quarto> filtrar(Quarto quarto){
		return quartoDAO.filtrar(quarto);
	}		
}

package br.unipe.cc.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.Hospede;
import br.unipe.cc.model.Quarto;
import br.unipe.cc.model.Reserva;
import br.unipe.cc.model.DAO.ReservaDAO;

@Service
@Transactional
public class ReservaService {
	
	@Autowired
	private ReservaDAO reservaDAO;
	
	@Autowired
	private HospedeService hospedeService;
	
	@Autowired
	private QuartoService quartoService;
	
	public void efetuarReserva(Reserva reserva){
		Hospede hospede = hospedeService.buscarPorId(reserva.getHospede().getId());
		Quarto quarto = quartoService.buscarPorId(reserva.getQuarto().getId());
		reserva.setHospede(hospede);
		reserva.setQuarto(quarto);		
		if(reserva.getQuarto() == null){
			throw new RuntimeException("Quarto nao cadastrado");
		}
		
		if (reserva.getHospede().hasValidID()){
			throw new RuntimeException("Hospede n�o cadastrado");
		}		
		reservaDAO.salvar(reserva);
	}
	
	public void alterStatusReserva(Reserva reserva){
		ReservaDAO reservaDAO = new ReservaDAO();
		
		if(reserva.getQuarto() == null){
			throw new RuntimeException("Quarto nao cadastrado");
		}
		
		if (reserva.getHospede().hasValidID()){
			throw new RuntimeException("Hospede n�o cadastrado");
		}
		
		if (reservaDAO.alterStatus(reserva) > 1){
			/* TODO Verificar com o Professor sobre o procedimento de rollback */
			//manager.getTransaction().rollback();
			throw new RuntimeException("Multiplos PK");
		}
	}
	
	public void checkNow(Reserva reserva){
		ReservaDAO reservaDAO = new ReservaDAO();
		
		if(reserva.getQuarto() == null){
			throw new RuntimeException("Quarto nao cadastrado");
		}
		
		if (reserva.getHospede().hasValidID()){
			throw new RuntimeException("Hospede n�o cadastrado");
		}
		
		if (reservaDAO.alterCheck(reserva) > 1){
			/* TODO Verificar com o Professor sobre o procedimento de rollback */
			//manager.getTransaction().rollback();
			throw new RuntimeException("Multiplos PK");
		}
	}
	
	public void efetuarPagamento(Reserva reserva){
		ReservaDAO reservaDAO = new ReservaDAO();
		
		if(reserva.getQuarto() == null){
			throw new RuntimeException("Quarto nao cadastrado");
		}
		
		if (reserva.getHospede().hasValidID()){
			throw new RuntimeException("Hospede n�o cadastrado");
		}
		
		if (reservaDAO.novoPagamento(reserva) > 1){
			/* TODO Verificar com o Professor sobre o procedimento de rollback */
			//manager.getTransaction().rollback();
			throw new RuntimeException("Multiplos PK");
		}
	}
	
	public void listarQuartosDisponiveis(){
		//TODO
	}
	
	public void listarReservasHospedes(){
		//TODO
	}

	public void remover(Reserva reserva){
		reservaDAO.remover(reserva);
	}	

	public Reserva buscarPorId(long id){
		return reservaDAO.buscarPorId(id);
	}	
		
	public List<Reserva> listar(){
		return reservaDAO.listar();
	}	

	public void atualizar(Reserva reserva){
		reservaDAO.atualizar(reserva);		
	}	
	
	/*public void atualizar(Reserva hospede){
		ReservaDAO reservaDAO = new ReservaDAO(manager);
		
		if(reserva.getUsuario() == null || reserva.getUsuario().hasValidID()){
			throw new RuntimeException("Usuario nao cadastrado");
		}
		
		hospedeDAO.atualizar(hospede);
		
		manager.getTransaction().begin();
		manager.getTransaction().commit();	
	}
	
	public void remover(Hospede hospede){
		HospedeDAO hospedeDAO = new HospedeDAO(manager);
		hospedeDAO.remover(hospede);
		
		manager.getTransaction().begin();
		manager.getTransaction().commit();
	}
	

	public List<Hospede> listar(){
		Query query = manager.createQuery("select c from " + entityClass().getSimpleName() + " c");
		return query.getResultList();
	}	

*/
	
}

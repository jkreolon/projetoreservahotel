package br.unipe.cc.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.unipe.cc.model.Consumo;
import br.unipe.cc.model.DAO.ConsumoDAO;

@Service
@Transactional
public class ConsumoService {
	
	@Autowired
	ConsumoDAO consumoDAO;
	
	public void salvar(Consumo consumo){
		ConsumoDAO consumoDAO = new ConsumoDAO();
		
		if(consumo.getProduto() == null || consumo.getProduto().hasValidID()){
			throw new RuntimeException("Produto inexistente");
		}	
		consumoDAO.salvar(consumo);
	}
	
/*	
	public List<Quarto> listar(){
		QuartoDAO quartoDAO = new QuartoDAO(manager);
		return quartoDAO.listar();
	}

	public void atualizar(Quarto quarto){
		QuartoDAO quartoDAO = new QuartoDAO(manager);
		
		if(quarto.getCategoria() == null || quarto.getCategoria().hasValidID()){
			throw new RuntimeException("Quarto sem categoria");
		}
		
		quartoDAO.atualizar(quarto);
		
		manager.getTransaction().begin();
		manager.getTransaction().commit();		
	}	
	
	public void remover(Quarto quarto){
		QuartoDAO quartoDAO = new QuartoDAO(manager);
		quartoDAO.remover(quarto);
		
		manager.getTransaction().begin();
		manager.getTransaction().commit();
	}
*/	
}

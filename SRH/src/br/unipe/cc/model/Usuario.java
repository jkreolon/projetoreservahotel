package br.unipe.cc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

@Entity
@SequenceGenerator(name = "usuario_id",sequenceName= "seq_usuario",allocationSize=1)
public class Usuario extends AbstractEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="usuario_id")
	private long id;
	
	@NotNull(message="O login n�o pode ser vazio")
	private String login;
	
	@NotNull(message="A senha n�o pode ser vazia")
	private String senha;
	
	public Usuario() {
		super();
	}

	public Usuario(long id) {
		super();
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}

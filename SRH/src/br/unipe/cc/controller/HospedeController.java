package br.unipe.cc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.Endereco;
import br.unipe.cc.model.Hospede;
import br.unipe.cc.model.Usuario;
import br.unipe.cc.model.service.HospedeService;
import br.unipe.cc.model.service.UsuarioService;


@Controller
@RequestMapping(value="/hospede")
public class HospedeController {
	
	@Autowired
	private HospedeService hospedeService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value="listar", method=RequestMethod.GET)
	public String listar(ModelMap map){
		List<Hospede> hospedes = hospedeService.listar();	
		map.addAttribute("hospedes", hospedes);
		map.addAttribute("filtro", new Hospede());
		return "hospede/listar";
	}
	
	@RequestMapping(value="{id}/formDetail", method=RequestMethod.GET)
	public String formDetail(@PathVariable("id") Long id, ModelMap map){
		Hospede hospede = hospedeService.buscarPorId(id);
		map.addAttribute("hospede", hospede);
		return "hospede/detalhar";
	}
	
	@RequestMapping(value="{id}/remover", method=RequestMethod.GET)
	public String remover(@PathVariable Long id){
		hospedeService.remover(new Hospede(id));
		return "redirect:/hospede/listar";
	}
	
	@RequestMapping(value="form", method=RequestMethod.GET)
	public String form(ModelMap map){
		Map<Long, String> selectUsuario = selectUsuario();
		Hospede hospede = new Hospede();
		hospede.setUsuario(new Usuario());
		hospede.setEndereco(new Endereco());
		map.addAttribute("selectUsuario",selectUsuario);				
		map.addAttribute("hospede", hospede);
		return "hospede/novo";
	}
	
	@RequestMapping(value="{id}/form", method=RequestMethod.GET)
	public String formUpdate(@PathVariable("id") Long id, ModelMap map){
		Hospede hospede = hospedeService.buscarPorId(id);
		map.addAttribute("hospede", hospede);
		return "hospede/update";
	}

	@RequestMapping(value="pesquisalike", method=RequestMethod.GET)
	public String formPesquisa(@ModelAttribute("filtro") Hospede filtro, ModelMap map){
		List<Hospede> hospedes = hospedeService.filtrar(filtro);	
		map.addAttribute("hospedes", hospedes);
		map.addAttribute("filtro", filtro);
		return "hospede/listar";
	}	
	
	@RequestMapping(value="salvar", method=RequestMethod.POST)
	public String salvar(@ModelAttribute("hospede") @Valid Hospede hospede, BindingResult result, ModelMap map ){
		if(result.hasErrors()){
			map.addAttribute("hospede", hospede);
			return "hospede/novo";
		}
		hospedeService.salvar(hospede);
		return "redirect:/hospede/listar";
	}
	
	
	
	@RequestMapping(value="update", method=RequestMethod.POST)
	public String update(@ModelAttribute("hospede") @Valid Hospede hospede, BindingResult result, ModelMap map ){
		if(result.hasErrors()){
			map.addAttribute("hospede", hospede);
			return "hospede/update";
		}
		hospedeService.atualizar(hospede);
		return "redirect:/hospede/listar";
	}
	
	public Map<Long, String> selectUsuario(){
		
		List<Usuario> usuarios = usuarioService.listar();
		Map<Long, String> mapa = new HashMap<Long, String>();
		
		for(Usuario usuario:usuarios){
			mapa.put(usuario.getId(),usuario.getLogin());
		}
		return mapa;
	}	
	

}



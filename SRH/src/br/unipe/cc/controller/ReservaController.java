package br.unipe.cc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.unipe.cc.model.Hospede;
import br.unipe.cc.model.Quarto;
import br.unipe.cc.model.Reserva;
import br.unipe.cc.model.service.HospedeService;
import br.unipe.cc.model.service.QuartoService;
import br.unipe.cc.model.service.ReservaService;

@Controller
@RequestMapping(value="/reserva")
public class ReservaController {
	
	@Autowired
	private ReservaService reservaService;
	
	@Autowired
	private HospedeService hospedeService;
	
	@Autowired
	private QuartoService quartoService;
	
	@RequestMapping(value="listar", method=RequestMethod.GET)
	public String listar(ModelMap map){
		Map<Long, String> selectHospede = selectHospede();
		Map<Long, String> selectQuarto = selectQuarto();		
		List<Reserva> reservas = reservaService.listar();	
		map.addAttribute("reservas", reservas);
		map.addAttribute("filtro", new Reserva());
		map.addAttribute("selectHospede",selectHospede);
		map.addAttribute("selectQuarto",selectQuarto);			
		return "reserva/listar";
	}
	
	@RequestMapping(value="{id}/remover", method=RequestMethod.GET)
	public String remover(@PathVariable Long id, ModelMap map){
		Reserva reserva = reservaService.buscarPorId(id);
		try{
			reservaService.remover(reserva);
		}catch(Exception e){	
			map.addAttribute("msg", e.getMessage());
		}
		return "forward:/reserva/listar";
	}
	
	
	@RequestMapping(value="form", method=RequestMethod.GET)
	public String form(ModelMap map){
		Map<Long, String> selectHospede = selectHospede();
		Map<Long, String> selectQuarto = selectQuarto();
		Reserva reserva = new Reserva();
		reserva.setHospede(new Hospede());
		reserva.setQuarto(new Quarto());
		map.addAttribute("selectHospede",selectHospede);
		map.addAttribute("selectQuarto",selectQuarto);
		map.addAttribute("reserva", reserva);
		return "reserva/novo";
	}
	
	@RequestMapping(value="{id}/form", method=RequestMethod.GET)
	public String formUpdate(@PathVariable("id") Long id, ModelMap map){
		Map<Long, String> selectHospede = selectHospede();
		Map<Long, String> selectQuarto = selectQuarto();		
		Reserva reserva = reservaService.buscarPorId(id);
		map.addAttribute("selectHospede",selectHospede);
		map.addAttribute("selectQuarto",selectQuarto);		
		map.addAttribute("reserva", reserva);
		return "reserva/update";
	}

	@RequestMapping(value="pesquisalike", method=RequestMethod.GET)
	public String formPesquisa(@ModelAttribute("filtro") Reserva filtro, ModelMap map){
		List<Reserva> reservas = reservaService.listar();//TODO tratar o metodo listarquartosdisponiveis	
		map.addAttribute("reservas", reservas);
		map.addAttribute("filtro", filtro);
		return "reserva/listar";
	}	
	
	@RequestMapping(value="salvar", method=RequestMethod.POST)
	public String salvar(@ModelAttribute("reserva") @Valid Reserva reserva, BindingResult result, ModelMap map ){
		if(result.hasErrors()){
			map.addAttribute("reserva", reserva);
			return "reserva/novo";
		}
		reservaService.efetuarReserva(reserva);
		return "redirect:/reserva/listar";
	}
			
	@RequestMapping(value="update", method=RequestMethod.POST)
	public String update(@ModelAttribute("reserva") @Valid Reserva reserva, BindingResult result, ModelMap map ){
		if(result.hasErrors()){
			map.addAttribute("reserva", reserva);
			return "reserva/update";
		}
		reservaService.atualizar(reserva);
		return "redirect:/reserva/listar";
	}
	

	public Map<Long, String> selectHospede(){
		
		List<Hospede> hospedes = hospedeService.listar();
		Map<Long, String> mapa = new HashMap<Long, String>();
		
		for(Hospede hospede:hospedes){
			mapa.put(hospede.getId(),hospede.getNome());
		}
		return mapa;
	}	

	public Map<Long, String> selectQuarto(){
		
		List<Quarto> quartos = quartoService.listar();
		Map<Long, String> mapa = new HashMap<Long, String>();
		
		for(Quarto quarto:quartos){
			mapa.put(quarto.getId(),quarto.getAndar() + " | " + quarto.getNumero());
		}
		return mapa;
	}
	
}



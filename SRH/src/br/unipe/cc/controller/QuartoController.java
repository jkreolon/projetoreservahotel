package br.unipe.cc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.Quarto;
import br.unipe.cc.model.service.CategoriaService;
import br.unipe.cc.model.service.QuartoService;


@Controller
@RequestMapping(value="/quarto")
public class QuartoController {
	
	@Autowired
	private QuartoService quartoService;
	
	@Autowired
	private CategoriaService categoriaService;
	
	@RequestMapping(value="listar", method=RequestMethod.GET)
	public String listar(ModelMap map){
		List<Quarto> quartos = quartoService.listar();	
		map.addAttribute("quartos", quartos);
		map.addAttribute("filtro", new Quarto());
		return "quarto/listar";
	}
	
	@RequestMapping(value="{id}/remover", method=RequestMethod.GET)
	public String remover(@PathVariable Long id){
		quartoService.remover(new Quarto(id));
		return "redirect:/quarto/listar";
	}
	
	@RequestMapping(value="form", method=RequestMethod.GET)
	public String form(ModelMap map){
		Quarto quarto = new Quarto();
		quarto.setCategoria(new Categoria());
		Map<Long, String> selectCategoria = selectCategoria();
		map.addAttribute("selectCategoria",selectCategoria);
		map.addAttribute("quarto", quarto);
		return "quarto/novo";
	}
	
	@RequestMapping(value="{id}/form", method=RequestMethod.GET)
	public String formUpdate(@PathVariable("id") Long id, ModelMap map){
		Quarto quarto = quartoService.buscarPorId(id);
		Map<Long, String> selectCategoria = selectCategoria();
		map.addAttribute("selectCategoria",selectCategoria);		
		map.addAttribute("quarto", quarto);
		return "quarto/update";
	}

	@RequestMapping(value="pesquisalike", method=RequestMethod.GET)
	public String formPesquisa(@ModelAttribute("quartos") Quarto filtro, ModelMap map){
		List<Quarto> quartos = quartoService.filtrar(filtro);	
		map.addAttribute("quartos", quartos);
		map.addAttribute("filtro", filtro);
		return "quarto/listar";
	}	
	
	
	@RequestMapping(value="salvar", method=RequestMethod.POST)
	public String salvar(@ModelAttribute("quarto") @Valid Quarto quarto , BindingResult result, ModelMap map ){
		if(result.hasErrors()){
			map.addAttribute("quarto", quarto);
			return "quarto/novo";
		}
		quartoService.salvar(quarto);
		return "redirect:/quarto/listar";
	}
	
		
	@RequestMapping(value="update", method=RequestMethod.POST)
	public String update(@ModelAttribute("categoria") @Valid Quarto quarto, BindingResult result, ModelMap map ){
		if(result.hasErrors()){
			map.addAttribute("quarto", quarto);
			return "quarto/update";
		}
		quartoService.atualizar(quarto);
		return "redirect:/quarto/listar";
	}
	
	public Map<Long, String> selectCategoria(){
		
		List<Categoria> categorias = categoriaService.listar();
		Map<Long, String> mapa = new HashMap<Long, String>();
		
		for(Categoria categoria:categorias){
			mapa.put(categoria.getId(),categoria.getNome());
		}
		return mapa;
	}	

}



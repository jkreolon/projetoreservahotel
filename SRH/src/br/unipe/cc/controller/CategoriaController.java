package br.unipe.cc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.service.CategoriaService;

@Controller
@RequestMapping(value="/categoria")
public class CategoriaController {
	
	@Autowired
	private CategoriaService categoriaService;
	
	@RequestMapping(value="listar", method=RequestMethod.GET)
	public String listar(ModelMap map){
		List<Categoria> categorias = categoriaService.listar();	
		map.addAttribute("categorias", categorias);
		map.addAttribute("filtro", new Categoria());
		return "categoria/listar";
	}
	
	@RequestMapping(value="{id}/remover", method=RequestMethod.GET)
	public String remover(@PathVariable Long id, ModelMap map){
		try{
			categoriaService.remover(new Categoria(id));
		}catch(Exception e){	
			map.addAttribute("msg", e.getMessage());
		}
		return "forward:/categoria/listar";
	}
	
	@RequestMapping(value="form", method=RequestMethod.GET)
	public String form(ModelMap map){
		Categoria categoria = new Categoria();
		map.addAttribute("categoria", categoria);
		return "categoria/novo";
	}
	
	@RequestMapping(value="{id}/form", method=RequestMethod.GET)
	public String formUpdate(@PathVariable("id") Long id, ModelMap map){
		Categoria categoria = categoriaService.buscarPorId(id);
		map.addAttribute("categoria", categoria);
		return "categoria/update";
	}

	@RequestMapping(value="pesquisalike", method=RequestMethod.GET)
	public String formPesquisa(@ModelAttribute("filtro") Categoria filtro, ModelMap map){
		List<Categoria> categorias = categoriaService.filtrar(filtro);	
		map.addAttribute("categorias", categorias);
		map.addAttribute("filtro", filtro);
		return "categoria/listar";
	}	
	
	@RequestMapping(value="salvar", method=RequestMethod.POST)
	public String salvar(@ModelAttribute("categoria") @Valid Categoria categoria, BindingResult result, ModelMap map ){
		if(result.hasErrors()){
			map.addAttribute("categoria", categoria);
			return "categoria/novo";
		}
		categoriaService.salvar(categoria);
		return "redirect:/categoria/listar";
	}
	
	
	
	@RequestMapping(value="update", method=RequestMethod.POST)
	public String update(@ModelAttribute("categoria") @Valid Categoria categoria, BindingResult result, ModelMap map ){
		if(result.hasErrors()){
			map.addAttribute("categoria", categoria);
			return "categoria/update";
		}
		categoriaService.atualizar(categoria);
		return "redirect:/categoria/listar";
	}
	

}



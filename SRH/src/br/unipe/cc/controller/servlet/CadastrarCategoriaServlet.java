package br.unipe.cc.controller.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.service.CategoriaService;

/**
 * Servlet implementation class CadastrarCategoriaServlet
 */
public class CadastrarCategoriaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	@Autowired
    private CategoriaService categoriaService;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nome = request.getParameter("nome");
		String capacidade = request.getParameter("capacidade");
		String valor = request.getParameter("valor");
		String id = request.getParameter("id");
		

		CategoriaService categoriaService = new CategoriaService();
		
		Categoria categoria = null;
		if(id != null && !id.equals("")){
			categoria = categoriaService.buscarPorId(new Long(id));
		}
		else{
			categoria = new Categoria();
		}
		
		categoria.setNome(nome);
		categoria.setCapacidade(new Integer(capacidade));
		categoria.setValor(Float.parseFloat(valor));
	
		if(id != null && !id.equals("")){
			categoriaService.atualizar(categoria);
			request.setAttribute("msg", "Categoria atualizada com sucesso");
		}
		else{
			categoriaService.salvar(categoria);
			request.setAttribute("msg", "Categoria cadastrada com sucesso");
		}
		
		request.getRequestDispatcher("listarCategoria.do").forward(request, response);
		
	}

}



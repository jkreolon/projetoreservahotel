package br.unipe.cc.controller.servlet;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.service.CategoriaService;

/**
 * Servlet implementation class ListarCategoriaServlet
 */
public class ListarCategoriaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarCategoriaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	static EntityManagerFactory fac = null;
	
	public void init() throws ServletException {
		fac = Persistence.createEntityManagerFactory("SRH");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EntityManager manager = fac.createEntityManager(); 
		
		CategoriaService categoriaService = new CategoriaService();
		
		List<Categoria> categorias =  categoriaService.listar();
		
		request.setAttribute("categorias", categorias);		
		
		request.getRequestDispatcher("listarCategoria.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

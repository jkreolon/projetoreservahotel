package br.unipe.cc.controller.servlet;

import java.io.IOException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import br.unipe.cc.model.Usuario;
import br.unipe.cc.model.service.UsuarioService;


/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	static EntityManagerFactory fac = null;
	
	public void init() throws ServletException {
		fac = Persistence.createEntityManagerFactory("SRH");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		
		if(login.equals("") || senha.equals("")){
			request.setAttribute("msgErro", "Login ou senha n�o preenchidos");
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		
		EntityManager manager = fac.createEntityManager(); 
		UsuarioService usuarioService = new UsuarioService();
		
		Usuario usuario = usuarioService.efetuarLogin(login, senha);
		if(usuario == null){
			request.setAttribute("msgErro", "Login ou senha INV�LIDOS");
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		
		request.getSession().setAttribute("usuario", usuario);
		
		
		request.getRequestDispatcher("main.jsp").forward(request, response);
		manager.close();
	}
}

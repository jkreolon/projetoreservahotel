package br.unipe.cc.controller.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import br.unipe.cc.model.Categoria;
import br.unipe.cc.model.service.CategoriaService;

/**
 * Servlet implementation class NovaCategoriaServlet
 */
public class NovaCategoriaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	static EntityManagerFactory fac = null;
	
	public void init() throws ServletException {
		fac = Persistence.createEntityManagerFactory("SRH");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		
		if(id != null){
			EntityManager manager = fac.createEntityManager(); 
			CategoriaService categoriaService = new CategoriaService();
			Categoria categoria = categoriaService.buscarPorId(new Long(id));
			request.setAttribute("categoria", categoria);
			
		}
		
		request.getRequestDispatcher("novaCategoria.jsp").forward(request, response);
		
	}

}
